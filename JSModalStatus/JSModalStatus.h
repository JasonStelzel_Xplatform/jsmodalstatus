//
//  JSModalStatus.h
//  JSModalStatus
//
//  Created by Jason Stelzel on 6/21/19.
//  Copyright © 2019 Jason Stelzel. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JSModalStatus.
FOUNDATION_EXPORT double JSModalStatusVersionNumber;

//! Project version string for JSModalStatus.
FOUNDATION_EXPORT const unsigned char JSModalStatusVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JSModalStatus/PublicHeader.h>

